/**
 * 
 */
package org.galaxyupdater;

import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;

import org.galaxyupdater.gui.MainInterface;
import org.galaxyupdater.interconnect.GalaxyConnector;
import org.galaxyupdater.interconnect.Game;
import org.galaxyupdater.interconnect.GameVersion;
import org.galaxyupdater.log.LogManager;

import gnu.getopt.Getopt;
import gnu.getopt.LongOpt;

/**
 * Launcher
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * @since 30/10/2015 17:15:51
 *
 */
public class Launcher {
	/** Enumeration of supported Operating Systems */
	public enum OperatingSystem {
		/** Unknown operating system */
		UNKNOWN (-1, "Unknown OS"),
		/** Windows */
		WINDOWS (0, "Windows"),
		/** OSX */
		OSX (1, "OSX"),
		/** Linux */
		LINUX (2, "Linux");
		
		/** Operating System id */
		private int id;
		/** Operating System name */
		private String name;
		
		/**
		 * Creates a new Operating System enumeration item.
		 * @param id OS identifier
		 * @param name OS name
		 */
		OperatingSystem(int id, String name) {
			this.id = id;
			this.name = name;
		}
		
		/**
		 * Gets the Operating System identifier.
		 * @return Identifier
		 */
		public int getId() { return id; }
		
		/**
		 * Gets the Operating System name.
		 * @return Name
		 */
		public String getName() { return name; }
		
		/**
		 * Resolve an Operating System identifier to an enumeration item. 
		 * @param id Identifier
		 * @return Operating System instance or UNKNOWN
		 */
		public static OperatingSystem fromId(int id) {
			for (OperatingSystem system: values()) {
				if (system.id == id) return system;
			}
			return OperatingSystem.UNKNOWN;
		}
	}
	/** Operating Systems */
	private static OperatingSystem os = null;
	/** Instance */
	private static Launcher instance = null;
	/** Flag that indicates whether or not we are running headless */
	private boolean isHeadless;
	/** GUI */
	private MainInterface gui;

	/**
	 * Creates a new instance of the Launcher.
	 */
	private Launcher() {
		gui = null;
		isHeadless = true;
		initOperatingSystem();
	}

	/**
	 * Gets the current Launcher instance.
	 * @return Launcher instance
	 */
	public static Launcher getInstance() {
		if (instance == null)
			instance = new Launcher();
		return instance;
	}

	/**
	 * Method that returns the Current operating system.
	 * @return Running Operating System
	 */
	public static OperatingSystem getOperatingSystem() {
		if (os == null) initOperatingSystem();
		return os;
	}

	/**
	 * Flag that indicates whether or not we are running in headless mode.
	 * @return True if we are running in headless mode, false otherwise
	 */
	public boolean isHeadless() { return isHeadless; }
	
	/**
	 * Gets the current GUI.
	 * @return Current GUI or null if not started/headless
	 */
	public MainInterface getGUI() { return gui; }

	/**
	 * Method that starts and shows the GUI.
	 */
	public void startGUI() {
		if (GraphicsEnvironment.isHeadless()) return; // can't start the gui
		if (gui != null) return; // already started, won't start again
		try {
			MainInterface.setLookAndFeel();
			gui = new MainInterface();
			gui.redirectSystemStreams();
			java.awt.EventQueue.invokeLater(new Runnable() {
				@Override
				public void run() {
					Launcher.getInstance().getGUI().setVisible(true);
				}
			});
		} catch (java.lang.InternalError e){
			System.err.println(e.getLocalizedMessage());
			gui = null;
			isHeadless = true;
		}
	}

	/**
	 * Method that closes the GUI.
	 */
	public void closeGUI() {
		if (gui == null) return;
		gui.setVisible(false);
		gui.dispose();
		gui = null;
	}

	/**
	 * Method that shows a terse help on the output and exits.
	 */
	public static void showHelp() {
		System.out.println("Usage: GalaxyUpdater [OPTIONS]");
		System.out.println();
		System.out.println("Where OPTIONS can be any (or none, defaults apply):");
		System.out.println("-h|--help: Show this short help and quit");
		System.out.println("-j|--jvm: Show JVM enviroment properties");
		System.out.println("-g ID|--game=ID: Sets game id to update");
		System.out.println("-r REVISION|--revision=REVISION: Sets the current game revision");
		System.out.println("-u|--update-to-latest: Update game to latest version (needs -g)");
		System.out.println("-l|--list: List tracked games and ids and exit (tab separated)");
		System.out.println("-x|--revisions: List revisions for selected game (needs -g)");
	}

	/**
	 * Method used to show JVM information.
	 */
	public static void showJVMInfo() {
		Properties jvmProps = System.getProperties();
		jvmProps.list(System.out);
	}

	/**
	 * Method that initializes the Operating System.
	 */
	private static void initOperatingSystem() {
		String OS = System.getProperty("os.name").toLowerCase();
		//String arch = System.getProperty("os.arch").toLowerCase();

		if (OS.indexOf("windows") >= 0) os = OperatingSystem.WINDOWS;
		else if (OS.indexOf("mac os x") >= 0) os = OperatingSystem.OSX;
		else if (OS.indexOf("linux") >= 0) os = OperatingSystem.LINUX;
		else os = OperatingSystem.UNKNOWN;
	}
	
	/**
	 * Method used to list tracked Games.
	 */
	private static void listGames() {
		GalaxyConnector conn = GalaxyConnector.getInstance();
		try {
			conn.updateData();
		} catch (IOException e) {
			System.err.println("Exception: "+e.getLocalizedMessage());
			System.exit(1);
		} catch (SQLException e) {
			System.err.println("Exception: "+e.getLocalizedMessage());
			System.exit(1);
		}
		for (Game game: conn.getInstalledGames())
			System.out.println(game.gameID+"\t"+game.name);
	}
	
	/**
	 * Method used to list revisions for a given game.
	 * @param gameid Game identifier
	 */
	private static void listRevisions(long gameid) {
		GalaxyConnector conn = GalaxyConnector.getInstance();
		try {
			conn.updateData();
		} catch (IOException e) {
			System.err.println("Exception: "+e.getLocalizedMessage());
			System.exit(1);
		} catch (SQLException e) {
			System.err.println("Exception: "+e.getLocalizedMessage());
			System.exit(1);
		}
		Game game = conn.getGame(gameid);
		if (game == null) {
			System.err.println("Game not found - List games and provide the actual game id");
			System.exit(2);
		}
		for (GameVersion version: game.availableVersions) {
			String published = "N/A";
			if (version.published != null)
				published = GameVersion.dateFormat.format(version.published.getTime());
			System.out.println(version.version+"\t"+version.versionName+" ("+published+")");
		}
	}
	
	/**
	 * Method used to set the current revision of a given game.
	 * @param gameid Game identifier
	 * @param revisionid Game revision to set
	 */
	private static void setRevision(long gameid, long revisionid) {
		GalaxyConnector conn = GalaxyConnector.getInstance();
		try {
			conn.updateData();
		} catch (IOException e) {
			System.err.println("Exception: "+e.getLocalizedMessage());
			System.exit(1);
		} catch (SQLException e) {
			System.err.println("Exception: "+e.getLocalizedMessage());
			System.exit(1);
		}
		try {
			conn.setVersion(gameid, revisionid);
		} catch (IOException e) {
			System.err.println("Exception: "+e.getLocalizedMessage());
			System.exit(1);
		} catch (SQLException e) {
			System.err.println("Exception: "+e.getLocalizedMessage());
			System.exit(1);
		} catch (IllegalArgumentException e) {
			System.err.println("Exception: "+e.getLocalizedMessage());
			System.exit(2);
		}
	}
	
	/**
	 * Method used to update a game to the latest revision of a given game.
	 * @param gameid Game identifier
	 */
	private static void updateToLatestRevision(long gameid) {
		GalaxyConnector conn = GalaxyConnector.getInstance();
		try {
			conn.updateData();
		} catch (IOException e) {
			System.err.println("Exception: "+e.getLocalizedMessage());
			System.exit(1);
		} catch (SQLException e) {
			System.err.println("Exception: "+e.getLocalizedMessage());
			System.exit(1);
		}
		Game game = conn.getGame(gameid);
		if (game == null) {
			System.err.println("Game not found - List games and provide the actual game id");
			System.exit(2);
		}
		try {
			conn.setVersion(gameid, game.getLatestVersionId());
		} catch (IOException e) {
			System.err.println("Exception: "+e.getLocalizedMessage());
			System.exit(1);
		} catch (SQLException e) {
			System.err.println("Exception: "+e.getLocalizedMessage());
			System.exit(1);
		} catch (IllegalArgumentException e) {
			System.err.println("Exception: "+e.getLocalizedMessage());
			System.exit(2);
		}
	}

	/**
	 * Default entry point.
	 * @param args Program arguments
	 */
	public static void main(String[] args) {
		boolean showGui = args.length == 0;
//		// TODO remove when GUI exists
//		if (args.length == 0) {
//			showHelp();
//			System.exit(0);
//		}
		long gameid = -1;
		long revisionid = -1;
		boolean listgames = false;
		boolean listrevisions = false;
		boolean updateToLatest = false;
		int c;
		String arg;
		StringBuffer sb = new StringBuffer();
		LongOpt[] longopts = new LongOpt[7];
		longopts[0] = new LongOpt("help", LongOpt.NO_ARGUMENT, null, (int)'h');
		longopts[1] = new LongOpt("jvm", LongOpt.NO_ARGUMENT, null, (int)'j');
		longopts[2] = new LongOpt("game", LongOpt.REQUIRED_ARGUMENT, sb, (int)'g');
		longopts[3] = new LongOpt("revision", LongOpt.REQUIRED_ARGUMENT, sb, (int)'r');
		longopts[4] = new LongOpt("games", LongOpt.NO_ARGUMENT, sb, (int)'l');
		longopts[5] = new LongOpt("revisions", LongOpt.NO_ARGUMENT, sb, (int)'x');
		longopts[6] = new LongOpt("update-latest-revision", LongOpt.NO_ARGUMENT, sb, (int)'u');

		Getopt g = new Getopt("GalaxyUpdater", args, "hjgrlxud", longopts);
		g.setOpterr(true); // We'll do our own error handling

		while ((c = g.getopt()) != -1) {
			switch (c) {
			case 0: // longopt with argument
				{
					arg = g.getOptarg();
					switch (g.getLongind()) {
						case 2:
							try {
								gameid = Long.parseLong(arg);
							} catch (NumberFormatException e) {
								e.printStackTrace();
							}
							break;
						case 3:
							try {
								revisionid = Long.parseLong(arg);
							} catch (NumberFormatException e) {
								e.printStackTrace();
							}
							break;
					}
				}
				break;
			case 'h':
				showHelp();
				System.exit(0);
				break;
			case 'l':
				listgames = true;
				break;
			case 'x':
				listrevisions = true;
				break;
			case 'u':
				updateToLatest = true;
				break;
			case 'd':
				showGui = false;
				break;
			case 'j':
				showJVMInfo();
				System.exit(0);
				break;
			case ':':
				System.out.println("You need an argument for option " +
						(char)g.getOptopt());
				break;
				//
			case '?':
				System.out.println("The option '" + (char)g.getOptopt() + 
						"' is not valid");
				break;
				//
			default:
				System.out.println("getopt() returned " + c);
				break;
			}
		}


		Launcher launcher = Launcher.getInstance();

		// test to see if we are running in headless mode
		if (showGui && GraphicsEnvironment.isHeadless())
			showGui = false;

		// Launch the GUI if asked for and possible
		if (showGui) {
			LogManager.writeLogMessage(Level.INFO, Launcher.class.getName(), "main()", "Launching GUI...", false, true);
			launcher.startGUI();
		} else {
			if (listgames) {
				listGames();
				System.exit(0);
			}

			if (listrevisions && gameid > -1) {
				listRevisions(gameid);
				System.exit(0);
			}
			
			if (revisionid > -1 && gameid > -1) {
				setRevision(gameid,revisionid);
				System.exit(0);
			}
			
			if (updateToLatest && gameid > -1) {
				updateToLatestRevision(gameid);
				System.exit(0);
			}
		}
		

	}
}
