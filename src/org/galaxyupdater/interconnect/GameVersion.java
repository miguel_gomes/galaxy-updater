/**
 * 
 */
package org.galaxyupdater.interconnect;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.galaxyupdater.Launcher.OperatingSystem;

/**
 * Game version.
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * @since 30/10/2015 18:45:08
 *
 */
public class GameVersion {
	/** Date format */
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
	/** To string format */
	public static final SimpleDateFormat toStringFormat = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm");
	
	/** Version id */
	public long id;
	/** Product */
	public Game product;
	/** OS */
	public OperatingSystem os;
	/** Version id */
	public long version;
	/** Version name */
	public String versionName;
	/** Branch */
	public String branch;
	/** Link to manifest */
	public String manifestLink;
	/** Whether or not the version is public */
	public boolean isPublic;
	/** Publishing date (maybe null if unknown) */
	public Calendar published;
	
	/**
	 * Game version constructor.
	 */
	public GameVersion() {
		isPublic = true;
		os = OperatingSystem.UNKNOWN;
	}
	
	/**
	 * Method used to read and set the published date in the supported date format.
	 * @param date Date to parse
	 * @throws ParseException If the date is not in the supported date format
	 */
	public void readPublishedDate(String date) throws ParseException {
		if (date == null || date.equals("null")) { 
			published = null;
			return;
		}
		Date parsedDate = dateFormat.parse(date);
		published = new GregorianCalendar();
		published.setTime(parsedDate);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String vname = "Unknown version";
		if (versionName != null && !versionName.equals("null")) vname = versionName;
		String _published = "";
		if (published != null)
			_published = " ("+GameVersion.toStringFormat.format(published.getTime())+")";
		return vname+_published;
	}
}
