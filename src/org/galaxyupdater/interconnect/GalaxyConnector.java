/**
 * 
 */
package org.galaxyupdater.interconnect;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.ImageIcon;

import org.galaxyupdater.Launcher;
import org.galaxyupdater.Launcher.OperatingSystem;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Galaxy Connector
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * @since 30/10/2015 17:38:15
 *
 */
public class GalaxyConnector {
	/** Instance */
	private volatile static GalaxyConnector instance;
	/** Index db name */
	public static final String indexDB = "index.db";
	/** Details db name */
	public static final String detailsDB = "productdetails.db";
	/** Revisions db name */
	public static final String revisionsDB = "rootmanifest.db";
	/** Product map (gameid, {@link Game}) */
	private HashMap<Long,Game> productMap;
	/** Storage path */
	private String storagePath = null;
	/** Index DB connection */
	private Connection indexConnection;
	/** Details DB connection */
	private Connection detailsConnection;
	/** Versions DB connection */
	private Connection versionsConnection;
	
	/**
	 * Private constructor to prevent instantiation.
	 */
	private GalaxyConnector() {
		OperatingSystem os = Launcher.getOperatingSystem();
		productMap = new HashMap<Long,Game>();
		switch (os) {
			case OSX:
				break;
			case LINUX: // currently there is no native linux client, so this is wine
			case WINDOWS:
				// need to figure out version, this will only work for Vista+
				storagePath = System.getenv("ProgramData");
				break;
			default:
				break;
		}
		if (storagePath != null) {
			storagePath += File.separatorChar + "GOG.com" + File.separatorChar + "Galaxy" + File.separatorChar + "storage" + File.separatorChar;
			try {
				updateData();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Gets a new instance of the Galaxy connector
	 * @return Instance
	 */
	public static GalaxyConnector getInstance() {
		if (instance == null)
			synchronized (GalaxyConnector.class) {
				if (instance == null)
					instance = new GalaxyConnector();
			}
		return instance;
	}
	
	/**
	 * Disconnects from all databases.
	 */
	public void disconnect() {
		if (indexConnection != null) {
			try {
				indexConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			indexConnection = null;
		}
		if (detailsConnection != null) {
			try {
				detailsConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			detailsConnection = null;
		}
		if (versionsConnection != null) {
			try {
				versionsConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			versionsConnection = null;
		}
	}
	
	/**
	 * Updates game data.
	 * @throws IOException If there was some issue locating the database files
	 * @throws SQLException If there was some issue opening the database files
	 */
	public void updateData() throws IOException, SQLException {
		disconnect();
		File indexFile = new File(storagePath+indexDB);
		if (!indexFile.exists() || !indexFile.isFile() || !indexFile.canRead())
			throw new IOException("Could not open INDEX db at: "+indexFile.getAbsolutePath());
		File detailsFile = new File(storagePath+detailsDB);
		if (!detailsFile.exists() || !detailsFile.isFile() || !detailsFile.canRead())
			throw new IOException("Could not open DETAILS db at: "+detailsFile.getAbsolutePath());
		File versionsFile = new File(storagePath+revisionsDB);
		if (!versionsFile.exists() || !versionsFile.isFile() || !versionsFile.canRead())
			throw new IOException("Could not open VERSIONS db at: "+versionsFile.getAbsolutePath());
		indexConnection = DriverManager.getConnection("jdbc:sqlite://"+indexFile.getAbsolutePath());
		detailsConnection = DriverManager.getConnection("jdbc:sqlite://"+detailsFile.getAbsolutePath());
		versionsConnection = DriverManager.getConnection("jdbc:sqlite://"+versionsFile.getAbsolutePath());
		updateProducts();
		updateDetails();
		updateVersions();
		postProcessData();
		disconnect(); // close connections
	}
	
	/**
	 * Method used to post process data, i.e. create relationships and what not.
	 */
	public void postProcessData() {
		for (Game game: productMap.values()) {
			if (!game.isStandalone) // set parent games for extras
				game.parentGame = productMap.get(game.productID);
			// handle versions
			game.currentVersion = game.getVersion(game.currentVersionId);
			if (game.currentVersionId == game.selectedRevision || game.currentVersionId == game.getLatestVersionId())
				game.selectedRevision = -1; // indicates that we are up to date
		}
	}
	
	/**
	 * Updates and creates available versions from the versions database.
	 * This method should only be executed after {@link #updateProducts()}
	 * @throws SQLException If there was some exception reading the data
	 */
	public void updateVersions() throws SQLException {
		if (versionsConnection == null) return;
		Statement stmt = versionsConnection.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT productId, manifest, timestamp FROM RootManifests");
		while (rs.next()) {
			long gameid = rs.getLong(1);
			Game game = productMap.get(gameid);
			if (game == null) continue;
			try {
				JSONObject manifest = new JSONObject(rs.getString(2));
				game.availableVersions.clear();
				// now the manifest has every thing we need to create game versions
				game.versionCount = manifest.getInt("total_count");
				JSONArray items = manifest.getJSONArray("items");
				for (int i = 0; i < items.length(); i++) {
					JSONObject item = items.getJSONObject(i);
					GameVersion version = new GameVersion();
					version.product = game;
					version.id = item.getLong("id");
					version.os = OperatingSystem.fromId(item.getInt("os"));
					version.isPublic = item.getBoolean("public");
					Object aux = item.get("branch");
					version.branch = aux != null ? aux.toString() : null;
					version.version = item.getLong("version");
					aux = item.get("version_name");
					version.versionName = aux != null ? aux.toString() : "Initial";
					version.manifestLink = item.getString("link").replace("\\/","/");
					try {
						aux = item.get("date_published");
						if (aux != null)
							version.readPublishedDate(aux.toString());
						else
							version.published = null;
					} catch (ParseException e) {
						e.printStackTrace();
					}
					game.availableVersions.add(version);
				}
				game.setLatestVersion();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		rs.close();
		// all done, a call to postProcessData should issue
		stmt.close();
	}
	
	/**
	 * Updates product details from the data in the details database.
	 * @throws SQLException If there was some exception reading the data
	 */
	public void updateDetails() throws SQLException {
		if (detailsConnection == null) return;
		Statement stmt = detailsConnection.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT gid, details, icon FROM ProductDetails");
		while(rs.next()) {
			long gameid = rs.getLong(1);
			Game game = productMap.get(gameid);
			if (game == null) continue;
			try {
				game.details = new JSONObject(rs.getString(2));
			} catch (JSONException e) {
				e.printStackTrace();
			}
			try {
				game.icon = new ImageIcon(new URL(rs.getString(3)));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}
		rs.close();
		// no close stuff
		stmt.close();
	}
	
	/**
	 * Updates products from the data in the index database.
	 * @throws SQLException If there was some exception reading the data
	 */
	public void updateProducts() throws SQLException {
		// And yes, I could have done this in a single massive query..
		productMap.clear();
		if (indexConnection == null) return;
		Statement stmt = indexConnection.createStatement();
		// first create the game instances
		ResultSet rs = stmt.executeQuery("SELECT gameID, productID, standalone FROM AvailableGameIDInfo");
		while (rs.next()) {
			Game game = new Game();
			game.gameID = rs.getLong(1);
			game.productID = rs.getLong(2);
			game.isStandalone = rs.getBoolean(3);
			productMap.put(game.gameID, game);
		}
		rs.close();
		// next grab locCode and name
		rs = stmt.executeQuery("SELECT gameID, locCode, name FROM AvailableGameIDNames");
		while (rs.next()) {
			long gameid = rs.getLong(1);
			Game game = productMap.get(gameid);
			if (game == null) 
				continue;
			game.locationCode = rs.getString(2);
			game.name = rs.getString(3);
		}
		rs.close();
		// now, grab languages
		rs = stmt.executeQuery("SELECT productId, language FROM AvailableLanguages");
		while (rs.next()) {
			long gameid = rs.getLong(1);
			Game game = productMap.get(gameid);
			game.languages.add(rs.getString(2));
		}
		rs.close();
		// next is installation details
		rs = stmt.executeQuery("SELECT productId, installationState, operationState, localpath, downloadsPath, supportPath, selectedTimestamp, selectedBranch, branchPassword, autoUpdateEnabled FROM Products");
		while (rs.next()) {
			long gameid = rs.getLong(1);
			Game game = productMap.get(gameid);
			if (game == null) 
				continue;
			game.installationState = InstallationState.fromId(rs.getInt(2));
			game.operationState = OperationState.fromId(rs.getInt(3));
			game.localPath = rs.getString(4);
			game.downloadsPath = rs.getString(5);
			game.supportPath = rs.getString(6);
			String rev = rs.getString(7);
			// rev is a bit strange..
			game.selectedRevision = rev != null ? Long.parseLong(rev) : -1;
			game.selectedBranch = rs.getString(8);
			game.branchPassword = rs.getString(9);
			game.autoUpdateEnabled = rs.getBoolean(10);
		}
		rs.close();
		// get the current version of each product
		rs = stmt.executeQuery("SELECT productId, version, branch FROM SyncInfo");
		while (rs.next()) {
			long gameid = rs.getLong(1);
			Game game = productMap.get(gameid);
			if (game == null) 
				continue;		
			game.currentVersionId = rs.getLong(2);
			game.currentBranch = rs.getString(3);
		}
		rs.close();
		stmt.close();
		// all done, a call to updateVersions should issue now
	}
	
	/**
	 * Gets a collection of installed games.
	 * @return Installed games list (no dlcs)
	 */
	public List<Game> getInstalledGames() {
		ArrayList<Game> games = new ArrayList<Game>();
		for (Game game: productMap.values()) {
			if (game.isStandalone) games.add(game);
		}
		return games;
	}
	
	/**
	 * Gets a specific game.
	 * @param gameid Game identifier
	 * @return Game or null if not found
	 */
	public Game getGame(long gameid) {
		return productMap.get(gameid);
	}
	
	/**
	 * Sets the current version of a given game in galaxy.
	 * @param gameid Game identifier
	 * @param versionid Version identifier
	 * @throws IOException If it was not possible to open the database file
	 * @throws SQLException If there was some issue while writing to the database
	 * @throws IllegalArgumentException If either the gameid or version id are invalid
	 */
	public void setVersion(long gameid, long versionid) throws IOException, SQLException {
		Game game = productMap.get(gameid);
		if (game == null) throw new IllegalArgumentException("Unknown game id");
		if (!game.isStandalone) throw new IllegalArgumentException("Only standalone games can be updated");
		GameVersion version = game.getVersion(versionid);
		if (version == null) throw new IllegalArgumentException("Version is invalid or not supported");
		
		// now we need to touch one database
		File indexFile = new File(storagePath+indexDB);
		if (!indexFile.exists() || !indexFile.isFile() || !indexFile.canRead())
			throw new IOException("Could not open INDEX db at: "+indexFile.getAbsolutePath());
		indexConnection = DriverManager.getConnection("jdbc:sqlite://"+indexFile.getAbsolutePath());
		// first we need to update the versions table
		
		PreparedStatement stmt = indexConnection.prepareStatement("UPDATE SyncInfo SET version=?, branch=? WHERE productId=?");
		stmt.setLong(1, version.version);
		stmt.setString(2, version.branch);
		stmt.setLong(3, gameid);
		stmt.executeUpdate();
		stmt.close();
		
		game.currentVersion = version;
		game.currentVersionId = versionid;
		
		stmt = indexConnection.prepareStatement("UPDATE Products SET selectedTimestamp=? WHERE productId=?");
		if (versionid == game.getLatestVersionId()) {
			stmt.setNull(1, java.sql.Types.BIGINT);
			game.selectedRevision = -1;
		} else {
			stmt.setLong(1, versionid);
			game.selectedRevision = versionid;
		}
		stmt.setLong(2, gameid);
		stmt.executeUpdate();
		indexConnection.close();
	}
}
