/**
 * 
 */
package org.galaxyupdater.interconnect;

/**
 * Operation state.
 * 
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * @since 30/10/2015 18:01:38
 *
 */
public enum OperationState {
	/** UNKNOWN - Unknown state */
	UNKNOWN(-1,"Unknown state"),
	/** OK */
	OK (0,"OK");
	
	/** Operation state id */
	private int id;
	/** Operation state description */
	private String description;
	
	/**
	 * Creates a new operation state enumeration item.
	 * @param id Identifier
	 * @param description Description
	 */
	OperationState(int id, String description) {
		this.id = id;
		this.description = description;
	}
	
	/**
	 * Gets the operation state identifier.
	 * @return Identifier
	 */
	public int getId () { return id; }
	
	/**
	 * Gets the operation state description.
	 * @return Description
	 */
	public String getDescription() { return description; }
	
	/**
	 * Resolves an operation state id to an enumeration instance.
	 * @param id Identifier
	 * @return Enumeration instance that matches the id or UNKNOWN
	 */
	public static OperationState fromId(int id) {
		for (OperationState state: values())
			if (state.id == id) return state;
		return UNKNOWN;
	}
}
