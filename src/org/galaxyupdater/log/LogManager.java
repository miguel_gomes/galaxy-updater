/**
 * 
 */
package org.galaxyupdater.log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * Simple Log Manager
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * @since 30/10/2015 17:34:21
 *
 */
public class LogManager {
	/** Log Date Format */
	protected static final SimpleDateFormat logDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss.SSS");
	/** LogManager instance */
	private static LogManager instance;
	/** Logger */
	private static Logger logger = Logger.getLogger("org.galaxyupdater.log");
	
	/**
	 * Creates a new instance of the LogManager.
	 */
	private LogManager() {
		for (Handler handler : logger.getHandlers()) {
			if (handler instanceof ConsoleHandler) {
				handler.setFormatter(new Formatter() {
					@Override
					public String format(LogRecord record) {
						StringBuilder ss = new StringBuilder();
						ss.append("[").append(record.getLevel().toString()).append("] ")
						.append(logDateFormat.format(new Date())).append(" (").append(record.getSourceClassName()).append(".").append(record.getSourceMethodName()).append(") ").append(record.getMessage());
						return ss.toString();
					}
				});
			}
		}
	}
	
	/**
	 * Adds a new Log Handler.
	 * @param handler Handler to add
	 * @return LogManager
	 */
	public LogManager addLogHandler(Handler handler) {
		if (handler != null) {
			logger.addHandler(handler);
		}
		return this;
	}
	
	/**
	 * Removes an existing Log Handler
	 * @param handler Handler to remove
	 * @return LogManager
	 */
	public LogManager removeLogHandler(Handler handler) {
		logger.removeHandler(handler);
		return this;
	}
	
	/**
	 * Gets the current instance of the LogManager.
	 * @return LogManager instance
	 */
	public static LogManager getInstance() {
		if (instance == null)
			instance = new LogManager();
		return instance;
	}
	
	/**
	 * Writes a log message.
	 * @param level Message level
	 * @param className Class name
	 * @param methodName Method name
	 * @param message Message text
	 * @see #writeLogMessage(Level, String, String, boolean, boolean)
	 */
	public static void writeLogMessage(Level level, String className, String methodName, String message) {
		writeLogMessage(level, className, methodName, message, false, false);
	}
	
	/**
	 * Writes a log message.
	 * @param level Message level
	 * @param className Class name
	 * @param methodName Method name
	 * @param message Message text
	 * @param append Whether or not we are going to append the message
	 * @param toSysOut Whether or not we should write this to System Out
	 */
	public static void writeLogMessage(Level level, String className, String methodName, String message, boolean append, boolean toSysOut) {
		if (toSysOut) {
			StringBuilder ss = new StringBuilder();
			ss.append(logDateFormat.format(new Date())).append(" (").append(className).append(".").append(methodName).append(") ").append(message);
			if (toSysOut) {
				if(!append)
					System.out.println("["+level.toString()+"] "+ss.toString());
				else
					System.out.print("["+level.toString()+"] "+ss.toString());
			}
		}
		logger.logp(level,className,methodName,message);
	}
}
