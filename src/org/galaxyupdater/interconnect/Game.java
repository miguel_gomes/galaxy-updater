/**
 * 
 */
package org.galaxyupdater.interconnect;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;

import org.json.JSONObject;

/**
 * Game instance.
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * @since 30/10/2015 17:49:05
 *
 */
public class Game {
	/** Game identifier */
	public long gameID;
	/** Product identifier */
	public long productID;
	/** Whether or not the product is a stand alone product */
	public boolean isStandalone;
	/** Location code */
	public String locationCode;
	/** Parent game if isStandalone == false */
	public Game parentGame;
	/** Associated extras */
	public List<Game> extras;
	/** Product name */
	public String name;
	/** Available languages */
	public List<String> languages;
	/** Installation state */
	public InstallationState installationState;
	/** Operation state */
	public OperationState operationState;
	/** Local path */
	public String localPath;
	/** Downloads path */
	public String downloadsPath;
	/** Support path */
	public String supportPath;
	/** Current version (-1 for current) */
	public long selectedRevision;
	/** Selected branch */
	public String selectedBranch;
	/** Selected branch password */
	public String branchPassword;
	/** Whether or not auto update is enabled */
	public boolean autoUpdateEnabled;
	/** Current game version id */
	public long currentVersionId;
	/** Latest game version id */
	private long latestVersionId;
	/** Current game version */
	public GameVersion currentVersion;
	/** Current branch */
	public String currentBranch;
	/** 
	 * Number of total versions a game has.
	 * This will not match available versions in some cases as there is no guarantee
	 * that all versions are kept on the servers. 
	 */
	public int versionCount;
	/** Available game versions */
	public List<GameVersion> availableVersions;
	/** Game icon */
	public ImageIcon icon;
	/** Game details (in JSON format) */
	public JSONObject details;
	
	/**
	 * Creates a new Game instance.
	 */
	public Game() {
		extras = new ArrayList<Game>();
		languages = new ArrayList<String>();
		availableVersions = new ArrayList<GameVersion>();
		installationState = InstallationState.UNKNOWN;
		operationState = OperationState.UNKNOWN;
		selectedRevision = -1;
		latestVersionId = -1;
	}
	
	/**
	 * Gets a specific game version.
	 * @param id Version id
	 * @return GameVersion or null if not found
	 */
	public GameVersion getVersion(long id) {
		for (GameVersion version: availableVersions)
			if (version.version == id) return version;
		return null;
	}
	
	/**
	 * Sets the latest version id.
	 * @return Latest version id
	 */
	public long setLatestVersion() {
		long latest = -1;
		for (GameVersion version: availableVersions)
			if (version.version > latest) latest = version.version;
		latestVersionId = latest;
		return latestVersionId;
	}
	
	/**
	 * Gets the latest version id.
	 * @return Identifier
	 */
	public long getLatestVersionId() {
		return latestVersionId;
	}
	
	/**
	 * Gets the latest version.
	 * @return Latest version or null if unknown
	 */
	public GameVersion getLatestVersion() {
		return getVersion(latestVersionId);
	}
	
	
}
