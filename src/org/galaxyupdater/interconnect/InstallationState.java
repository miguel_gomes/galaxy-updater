/**
 * 
 */
package org.galaxyupdater.interconnect;

/**
 * Installation State
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * @since 30/10/2015 17:53:31
 *
 */
public enum InstallationState {
	/** UNKNOWN - Unknown state */
	UNKNOWN(0,"Unknown state"),
	/** INSTALLED_VALID - Game is installed */
	INSTALLED_VALID (3,"Game is installed"),
	/** INSTALLED_INVALID - Game is installed but does not belong to account */
	INSTALLED_INVALID (2, "Game is installed but does not belong to account");
	
	/** Installation state id */
	private int id;
	/** Installation state description */
	private String description;
	
	/**
	 * Creates a new installation state enumeration item.
	 * @param id Identifier
	 * @param description Description
	 */
	InstallationState(int id, String description) {
		this.id = id;
		this.description = description;
	}
	
	/**
	 * Gets the installation state identifier.
	 * @return Identifier
	 */
	public int getId () { return id; }
	
	/**
	 * Gets the installation state description.
	 * @return Description
	 */
	public String getDescription() { return description; }
	
	/**
	 * Resolves an installation state id to an enumeration instance.
	 * @param id Identifier
	 * @return Enumeration instance that matches the id or UNKNOWN
	 */
	public static InstallationState fromId(int id) {
		for (InstallationState state: values())
			if (state.id == id) return state;
		return UNKNOWN;
	}
}
