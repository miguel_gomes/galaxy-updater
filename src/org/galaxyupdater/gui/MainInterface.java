/**
 * 
 */
package org.galaxyupdater.gui;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.galaxyupdater.Launcher;
import org.galaxyupdater.interconnect.GalaxyConnector;
import org.galaxyupdater.interconnect.Game;
import org.galaxyupdater.interconnect.GameVersion;
import org.galaxyupdater.log.LogManager;

import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.text.BadLocationException;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JList;

import net.miginfocom.swing.MigLayout;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.ListCellRenderer;
import javax.swing.JScrollPane;

/**
 * Interface Window
 * @author Miguel Gomes (alka.setzer@gmail.com)
 * @since 30/10/2015 17:19:38
 *
 */
public class MainInterface extends JFrame {
	/** Serial UID */
	private static final long serialVersionUID = 7580118789036861479L;
	
	private JLabel lblGame;
	private JComboBox<Game> jcb_game;
	private JLabel lblUpdate;
	private JComboBox<GameVersion> jcb_version;
	private JButton jb_update;
	private JLabel lblLog;
	private JTextArea jta_logArea;
	private GalaxyConnector connector;
	private List<Game> availableGames;
	private JScrollPane scrollPane;
	
	private class ComboBoxRenderer extends JLabel
	implements ListCellRenderer<Object> {
		/** UID */
		private static final long serialVersionUID = 2126137351891117656L;

		public ComboBoxRenderer() {
			setOpaque(true);
			setHorizontalAlignment(LEFT);
			setVerticalAlignment(CENTER);
		}

		/*
		 * This method finds the image and text corresponding
		 * to the selected value and returns the label, set up
		 * to display the text and image.
		 */
		public Component getListCellRendererComponent(
				JList<?> list,
				Object value,
				int index,
				boolean isSelected,
				boolean cellHasFocus) {
			if (isSelected) {
				setBackground(list.getSelectionBackground());
				setForeground(list.getSelectionForeground());
			} else {
				setBackground(list.getBackground());
				setForeground(list.getForeground());
			}

			//Set the icon and text.  If icon was null, say so.
			if (value == null) return this;
			Game game = (Game)value;
			if (game.icon != null)
				setIcon(game.icon);
			setText(game.name);

			return this;
		}
	}
	
	private class GameVersionComboBoxRenderer extends JLabel
	implements ListCellRenderer<Object> {
		/** UID */
		private static final long serialVersionUID = 2126137351891117656L;

		public GameVersionComboBoxRenderer() {
			setOpaque(true);
			setHorizontalAlignment(LEFT);
			setVerticalAlignment(CENTER);
		}

		/*
		 * This method finds the image and text corresponding
		 * to the selected value and returns the label, set up
		 * to display the text and image.
		 */
		public Component getListCellRendererComponent(
				JList<?> list,
				Object value,
				int index,
				boolean isSelected,
				boolean cellHasFocus) {
			if (isSelected) {
				setBackground(list.getSelectionBackground());
				setForeground(list.getSelectionForeground());
			} else {
				setBackground(list.getBackground());
				setForeground(list.getForeground());
			}

			//Set the icon and text.  If icon was null, say so.
			if (value == null) return this;
			GameVersion version = (GameVersion)value;
			boolean isCurrent = version == version.product.currentVersion;
			setText((isCurrent ? "* " : "")+version.toString());

			return this;
		}
	}
	
	/**
	 * Main interface.
	 */
	public MainInterface() {
		setLookAndFeel();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(new Dimension(450, 300));
		setTitle("Galaxy Updater");
		getContentPane().setLayout(new MigLayout("", "[27px][grow]", "[14px][][][grow]"));
		initComponents();
		connector = GalaxyConnector.getInstance();
		updateGamesList();
	}
	
	private void initComponents() {
		Container container = getContentPane();
		
		lblGame = new JLabel("Game");
		container.add(lblGame, "cell 0 0,alignx trailing,aligny top");
		
		jcb_game = new JComboBox<Game>();
		jcb_game.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateGameVersion();
			}
		});
		jcb_game.setToolTipText("Select game to update");
		ComboBoxRenderer renderer= new ComboBoxRenderer();
		renderer.setPreferredSize(new Dimension(64, 64));
		jcb_game.setRenderer(renderer);
		container.add(jcb_game, "cell 1 0,growx");
		
		lblUpdate = new JLabel("Version");
		container.add(lblUpdate, "cell 0 1,alignx trailing");
		
		jcb_version = new JComboBox<GameVersion>();
		jcb_version.setRenderer(new GameVersionComboBoxRenderer());
		jcb_version.setToolTipText("Select game version");
		container.add(jcb_version, "cell 1 1,growx");
		
		jb_update = new JButton("Update");
		jb_update.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Game game = (Game)jcb_game.getSelectedItem();
				GameVersion version = (GameVersion)jcb_version.getSelectedItem();
				if (game.currentVersion == version) return; // nothing to update
				LogManager.writeLogMessage(Level.INFO, MainInterface.class.getName(), "updateVersion()", "Updating "+game.name+" to version: "+version.toString(), false, true);
				try {
					connector.setVersion(game.gameID,version.version);
					LogManager.writeLogMessage(Level.INFO, MainInterface.class.getName(), "updateVersion()", "Updated "+game.name, false, true);
					updateGameVersion();
				} catch (IOException e1) {
					LogManager.writeLogMessage(Level.SEVERE, MainInterface.class.getName(), "updateVersion()", "Exception while updating version, cause: "+e1.getLocalizedMessage(), false, true);
					//e1.printStackTrace();
				} catch (SQLException e1) {
					LogManager.writeLogMessage(Level.SEVERE, MainInterface.class.getName(), "updateVersion()", "Exception while updating version, cause: "+e1.getLocalizedMessage(), false, true);
					//e1.printStackTrace();
				}
			}
		});
		container.add(jb_update, "cell 1 2,alignx right");
		
		lblLog = new JLabel("Log");
		container.add(lblLog, "cell 0 3,alignx right");
		
		scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, "cell 1 3,grow");
		
		jta_logArea = new JTextArea();
		scrollPane.setViewportView(jta_logArea);
		jta_logArea.setLineWrap(true);
		jta_logArea.setEditable(false);
		jta_logArea.setToolTipText("Log");
		jta_logArea.setRows(4);
	}
	
	/**
	 * Updates the game list.
	 */
	private void updateGamesList() {
		// remove all items
		jcb_game.removeAllItems();
		try {
			connector.updateData();
		} catch (IOException e) {
			e.printStackTrace();
			LogManager.writeLogMessage(Level.SEVERE, MainInterface.class.getName(), "updateGamesList()", "Exception while updating data, cause: "+e.getLocalizedMessage(), false, true);
		} catch (SQLException e) {
			e.printStackTrace();
			LogManager.writeLogMessage(Level.SEVERE, MainInterface.class.getName(), "updateGamesList()", "Exception while updating data, cause: "+e.getLocalizedMessage(), false, true);
		}
		availableGames = connector.getInstalledGames();
		for (Game game: availableGames)
			jcb_game.addItem(game);
		updateGameVersion();
	}
	
	private void updateGameVersion() {
		// first figure out what game is selected
		Game game = (Game)jcb_game.getSelectedItem();
		// then grab versions for that game
		jcb_version.removeAllItems();
		for (GameVersion version: game.availableVersions)
			jcb_version.addItem(version);
		if (game.getLatestVersion() != null)
			jcb_version.setSelectedItem(game.getLatestVersion());
	}
	
	/**
	 * Method used to update the text area with a given string.
	 *
	 * @param text String to update the text area
	 */
	private void updateTextArea(final String text) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				if (jta_logArea.getLineCount() == 999) {
					// remove first line
					try {
						jta_logArea.replaceRange(null, 0, jta_logArea.getLineEndOffset(0));
					} catch (BadLocationException e) {
						jta_logArea.setText("");
					}
				}
				jta_logArea.append(text);
			}
		});
	}
	
	/**
	 * Method used to redirect System streams to the log area.
	 */
	public void redirectSystemStreams() {
		OutputStream cout = new OutputStream() {
			OutputStream originalOut = System.out;

			@Override
			public void write(int b) throws IOException {
				updateTextArea(String.valueOf((char) b));
				originalOut.write(b);
			}

			@Override
			public void write(byte[] b, int off, int len) throws IOException {
				updateTextArea(new String(b, off, len));
				originalOut.write(b, off, len);
			}

			@Override
			public void write(byte[] b) throws IOException {
				write(b, 0, b.length);
				originalOut.write(b);
			}
		};
		OutputStream cerr = new OutputStream() {
			OutputStream originalOut = System.err;

			@Override
			public void write(int b) throws IOException {
				updateTextArea(String.valueOf((char) b));
				originalOut.write(b);
			}

			@Override
			public void write(byte[] b, int off, int len) throws IOException {
				updateTextArea(new String(b, off, len));
				originalOut.write(b, off, len);
			}

			@Override
			public void write(byte[] b) throws IOException {
				write(b, 0, b.length);
				originalOut.write(b);
			}
		};

		System.setOut(new PrintStream(cout, true));
		System.setErr(new PrintStream(cerr, true));
	}

	/**
	 * Sets the look and feel, the order is:
	 * <ol><li>System</li>
	 * <li>Nimbus</li>
	 * <li>Metal</li>
	 * <li>Panic</li>
	 * </ol>
	 */
	public static void setLookAndFeel() {
		try {
			// Check what is the System look and feel
			String crossLaf = UIManager.getCrossPlatformLookAndFeelClassName();
			String sysLaf = UIManager.getSystemLookAndFeelClassName();
			String laf = sysLaf;
			if (sysLaf.equals(crossLaf)) { // try again
				String nimbusLaf = null;
				String gtkLaf = null;
				String windowsLaf = null;
				for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
					if (info.getName().equals("Nimbus")) {
						nimbusLaf = info.getClassName();
					} else if (info.getName().equals("GTK+")) {
						gtkLaf = info.getClassName();
					} else if (info.getName().equals("Windows")) {
						windowsLaf = info.getClassName();
					}
				}
				switch (Launcher.getOperatingSystem()) {
				case WINDOWS:
					laf = (windowsLaf != null ? windowsLaf : (nimbusLaf != null ? nimbusLaf : sysLaf));
					break;
				case LINUX:
					laf = (gtkLaf != null ? gtkLaf : (nimbusLaf != null ? nimbusLaf : sysLaf));
					break;
				case OSX:
				default:
					laf = (nimbusLaf != null ? nimbusLaf : sysLaf);
					break;
				}
			}
			UIManager.setLookAndFeel(laf);
		} catch (Exception localException) {
			System.err.println("Exception: " + localException.getLocalizedMessage());
			try {
				UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			} catch (ClassNotFoundException e) {
			} catch (InstantiationException e) {
			} catch (IllegalAccessException e) {
			} catch (UnsupportedLookAndFeelException e) {
			}
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		setLookAndFeel();
		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new MainInterface().setVisible(true);
			}
		});
	}

}
